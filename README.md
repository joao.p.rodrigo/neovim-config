# Neovim Configuration

### Introduction

My Neovim configuration.

Based on:

* [NVIM-Lua Kickstart](https://github.com/nvim-lua/kickstart.nvim)
* [The Primeagen's Neovim config](https://github.com/ThePrimeagen/init.lua)
* [Dreams of Code Go config](https://github.com/ThePrimeagen/init.lua)

Snippets borrowed from:

* [rafamadriz/friendly-snippets](https://github.com/rafamadriz/friendly-snippets)

