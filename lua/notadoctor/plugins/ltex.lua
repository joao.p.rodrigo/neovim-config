function ToggleLtexLanguage(enabled, language)
	local ltexSettings = vim.lsp.get_clients({ name = "ltex" })[1].config.settings
	if ltexSettings == nil then
		return
	end

	if language ~= "" then
		ltexSettings.ltex.language = language
	else
		language = ltexSettings.ltex.language
	end

	ltexSettings.ltex.enabled = enabled

	vim.lsp.buf_notify(0, "workspace/didChangeConfiguration", { settings = ltexSettings })
	vim.api.nvim_echo({ { string.format("Ltex enabled=%s language=%s", tostring(enabled), language), "" } }, false, {})
end

return {
	ltex = {
		settings = {
			ltex = {
				enabled = false,
				language = "en-US",
				additionalRules = {
					languageModel = "~/models/ngrams/",
				},
			},
		},
		on_attach = function()
			vim.api.nvim_create_user_command("LtexOff", function()
				ToggleLtexLanguage(false, "")
			end, {})
			vim.api.nvim_create_user_command("LtexPT", function()
				ToggleLtexLanguage(true, "pt")
			end, {})
			vim.api.nvim_create_user_command("LtexEN", function()
				ToggleLtexLanguage(true, "en-US")
			end, {})
		end,
	},
}
